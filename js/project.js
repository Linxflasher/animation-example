$(document).ready(function () {

    // Имитация загрузки

    // setTimeout(function(){
    //     document.body.classList.add('loaded');
    // }, 3000);

    // Слайдер Пресса о нас

    $('.testimonial--slider').slick({
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                variableWidth: true
            }
        }]
    })

    // Вывод в label содержимого приатаченных через инпут файлов 

    let fileInputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(fileInputs, function (input) {
        let label = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener('change', function (e) {
            let fileName = '';
            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            else
                fileName = e.target.value.split('\\').pop();

            if (fileName)
                label.innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });
    });

    // Добавление класса если в поле есть контент

    const inputText = $(".input-field");
    const textareaField = $(".textarea-field");

    $(inputText).blur(function () {
        let inputValue = $(this).val();
        if (inputValue == '') {
            $(this).removeClass('not-empty');
        } else {
            $(this).addClass('not-empty');
        }
    });

    $(textareaField).blur(function () {
        let inputValue = $(this).val();
        if (inputValue == '') {
            $(this).removeClass('not-empty');
        } else {
            $(this).addClass('not-empty');
        }
    });

    // Боковое меню

    const shadowOverlay = document.getElementById("overlay");
    const sideNav = document.getElementById("side-nav");
    const navBtn = document.querySelector(".header--nav--btn");
    const closeNavBtn = document.getElementById("close-nav");

    navBtn.addEventListener("click", function () {
        shadowOverlay.classList.add("opened");
        sideNav.classList.add("opened");
        document.body.classList.add("modal-opened");
    })

    closeNavBtn.addEventListener("click", function () {
        shadowOverlay.classList.remove("opened");
        sideNav.classList.remove("opened");
        document.body.classList.remove("modal-opened");
    })


    shadowOverlay.addEventListener("click", function () {
        if (shadowOverlay.classList.contains("opened")) {
            shadowOverlay.classList.remove("opened");
            sideNav.classList.remove("opened");
            document.body.classList.remove("modal-opened");
        }
    });


    // Скролл навигации

    $.scrollIt({
        easing: 'ease-in', // the easing function for animation
        scrollTime: 500, // how long (in ms) the animation takes
        activeClass: 'active', // class given to the active nav element
        onPageChange: function () { // function(pageIndex) that is called when page is changed
            sideNav.classList.remove("opened");
            shadowOverlay.classList.remove("opened");
            document.body.classList.remove("modal-opened");
        },
        topOffset: -46 // offset (in px) for fixed top navigation
    });

    // Блок с картой

    const mapBlock = document.getElementById("map");
    const showMapMoscow = document.getElementById("map-moscow");
    const showMapOrel = document.getElementById("map-orel");
    const hideMapBtn = document.getElementById("close-map");
    const mapMoscow = document.querySelector(".map-moscow");
    const mapOrel = document.querySelector(".map-orel");
    const cityMoscow = document.querySelector(".city.moscow");
    const cityOrel = document.querySelector(".city.orel");

    showMapMoscow.addEventListener("click", function () {
        if (!showMapMoscow.classList.contains("btn-active")) {
            mapBlock.classList.add("map-opened");
            showMapMoscow.classList.add("btn-active");
            showMapMoscow.textContent = "Скрыть карту";
            mapMoscow.classList.add("show");
            cityMoscow.classList.add("active");

            mapOrel.classList.remove("show");
            cityOrel.classList.remove("active");
            showMapOrel.classList.remove("btn-active");
            showMapOrel.textContent = "Показать на карте";

        } else {
            mapBlock.classList.remove("map-opened");
            showMapMoscow.classList.remove("btn-active");
            showMapMoscow.textContent = "Показать на карте";
            mapMoscow.classList.remove("show");
            cityMoscow.classList.remove("active");
        }

    });

    showMapOrel.addEventListener("click", function () {
        if (!showMapOrel.classList.contains("btn-active")) {
            mapBlock.classList.add("map-opened");
            showMapOrel.classList.add("btn-active");
            showMapOrel.textContent = "Скрыть карту";
            mapOrel.classList.add("show");
            cityOrel.classList.add("active");

            mapMoscow.classList.remove("show");
            cityMoscow.classList.remove("active");
            showMapMoscow.classList.remove("btn-active");
            showMapMoscow.textContent = "Показать на карте";

        } else {
            mapBlock.classList.remove("map-opened");
            showMapOrel.classList.remove("btn-active");
            showMapOrel.textContent = "Показать на карте";
            mapOrel.classList.remove("show");
            cityOrel.classList.remove("active");
        }

    });

    hideMapBtn.addEventListener("click", function () {
        if (mapBlock.classList.contains("map-opened")) {
            mapBlock.classList.remove("map-opened");
            mapMoscow.classList.remove("show");
            mapOrel.classList.remove("show");
            showMapMoscow.classList.remove("btn-active");
            showMapOrel.classList.remove("btn-active");
            showMapMoscow.textContent = "Показать на карте";
            showMapOrel.textContent = "Показать на карте";
            cityMoscow.classList.remove("active");
            cityOrel.classList.remove("active");
        }
    })

    // Адаптив



    if (window.buffWidth <= 1024) {
        document.body.classList.add("mobile");

    } else {
        document.body.classList.remove("mobile");
    }

    // var $document = $(document),
    //     $headNav = $('#header'),
    //     className = 'hasScrolled';

    // $document.scroll(function () {
    //     $headNav.toggleClass(className, $document.scrollTop() >= 283);
    // });

    window.buffWidth = $('body').innerWidth();

    $(window).resize(function () {
        if (window.buffWidth != $('body').innerWidth()) {
            window.buffWidth = $('body').innerWidth();
            pageResizeEvents();
        }
    });

    pageResizeEvents();

    function pageResizeEvents() {

        // Проект Артхив карусель
        if (window.buffWidth <= 1140) {
            M.AutoInit();

        } else {
            M.AutoInit();
        }

        if (window.buffWidth <= 1024) {
            M.AutoInit();

        } else {
            M.AutoInit();
        }

        // Проект Артхив карусель End

        if (window.buffWidth <= 1024) {
            document.body.classList.add("mobile");

        } else {
            document.body.classList.remove("mobile");
        }
    }

    // Слайдер Digital-интегратор

    $('.mobile .digital--services').slick({
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true
    })



    // Слайдер Проекты

    $('.projects--list--mobile').slick({
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true
    })

    // Слайдер Клиенты

    $('.clients--list--mobile').slick({
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true
    })

    // Слайдер Команда

    $('.team--list--mobile').slick({
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true
    })

    // Анимация

    var triggerHeading = new ScrollTrigger({
        toggle: {
          visible: 'animation--fade-in-left-3d',
          hidden: 'animation--fade-out-left-3d'
        },
        once: true
      }, document.body, window);

});