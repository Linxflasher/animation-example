$(document).ready(function() {
  window.buffWidth = $("body").innerWidth();

  $(window).resize(function() {
    if (window.buffWidth != $("body").innerWidth()) {
      window.buffWidth = $("body").innerWidth();
      pageResizeEvents();
    }
  });

  pageResizeEvents();

  function pageResizeEvents() {
    // Проект Артхив карусель
    if (window.buffWidth <= 1140) {
      M.AutoInit();
    } else {
      M.AutoInit();
    }

    if (window.buffWidth <= 1024) {
      M.AutoInit();
    } else {
      M.AutoInit();
    }

    // Проект Артхив карусель End

    if (window.buffWidth <= 1024) {
      document.body.classList.add("mobile");
    } else {
      document.body.classList.remove("mobile");
    }
  }

  // Блок Функциональность

  const accordionItem = $("#accordion-list li");

  accordionItem.on("click", function() {
    if (!$(this).hasClass("active")) {
      accordionItem
        .removeClass("active")
        .find(".content")
        .slideUp(500);
      $(this)
        .addClass("active")
        .find(".content")
        .slideDown(500);

      var id = $(this).data("id");
      $(".functionality .carousel.selected").removeClass("selected");
      $("#" + id).addClass("selected");
      M.AutoInit();
    }
  });

  // Мобильные приложения Artchive скрытие/показ блоков

  $(".artchive-apps--list--item").on("mouseover", function() {
    $(".artchive-apps--list--item").removeClass("active");
    $(this).addClass("active");
    var id = $(this).data("id");
    $(".artchive-apps--slider.selected").removeClass("selected");
    $("#" + id).addClass("selected");
    M.AutoInit();
  });

  // Мобильные приложения Artchive - мобильная версия

  $(".mobile .artchive-apps--list--item").off("mouseover");

  $(".mobile .artchive-apps--list--item").on("click", function() {
    if (!$(this).hasClass("active")) {
      $(".artchive-apps--list--item").removeClass("active");
      $(".artchive-apps--slider-mobile").slideUp();

      $(this)
        .addClass("active")
        .closest("li")
        .find(".artchive-apps--slider-mobile")
        .slideDown();
      M.AutoInit();
    }
  });

  // Карусель функциональности приложений

  var elem = document.querySelector("#carousel");
  var instance = M.Carousel.init(elem);

  // Карусель Artchive Apps

  var elems = document.querySelectorAll(".carousel-app");
  var instances = M.Carousel.init(elems);

  // Рунет слайдер

  $(".runet-awards--slider").slick({
    dots: true,
    arrows: false,
    infinite: false,
    speed: 300,
    slidesToShow: 2,
    slidesToScroll: 1,
    variableWidth: true,
    focusOnSelect: true,
    infinite: true,
    responsive: [
      {
        breakpoint: 1800,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1024,
        settings: {
          variableWidth: true,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 500,
        settings: {
          variableWidth: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          speed: 600,
          cssEase: "ease-out"
        }
      }
    ]
  });
});
